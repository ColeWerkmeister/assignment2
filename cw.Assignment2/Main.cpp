#include <conio.h> //getch
#include <iostream> //cin/cout

using namespace std;

float Add(float num1, float num2);
float Subtract(float num1, float num2);
float Multiply(float num1, float num2);
float Divide(float num1, float num2);
float Again();

int main()
{

	float num1;
	float num2;
	char ope;

	cout << "Enter first number: ";
	cin >> num1;

	cout << "Enter second number: ";
	cin >> num2;

	cout << "Enter an operator (+, -, *, /): ";
	cin >> ope;

	if (ope == '+') 
	{
		Add(num1, num2);
	}
	else if (ope == '-') 
	{
		Subtract(num1, num2);
	}
	else if (ope == '*')
	{
		Multiply(num1, num2);
	}
	else if (ope == '/')
	{
		if (num1 == 0 || num2 == 0)
		{
			cout << "Cannot divide numbers by zero";
		}
		else
		{
			Divide(num1, num2);
		}
	}
	else
	{
		cout << "Invalid Operator";
		main();
	}

	_getch();
	return 0;
}


float Add(float num1, float num2)
{
	double ans = num1 + num2;
	cout << "Answer: ";
	cout << ans;
	cout << "\n";
	_getch();
	Again();
	return 0;
}

float Subtract(float num1, float num2)
{
	double ans = num1 - num2;
	cout << "Answer: ";
	cout << ans;
	cout << "\n";
	_getch();
	Again();
	return 0;
}

float Multiply(float num1, float num2)
{
	double ans = num1 * num2;
	cout << "Answer: ";
	cout << ans;
	cout << "\n";
	_getch();
	Again();
	return 0;
}

float Divide(float num1, float num2)
{
	double ans = num1 / num2;
	cout << "Answer: ";
	cout << ans;
	cout << "\n";
	_getch();
	Again();
	return 0;
}

float Again()
{
	char res;
	cout << "Would you like to do another calculation? \"Y\" or \"N\"\n";
	cin >> res;

	if (res == 'Y' || res == 'y')
	{
		main();
	}
		return 0;	
}